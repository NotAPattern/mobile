package com.example.lab7;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Bubble {
    private int x;
    private int y;
    private int size;
    private Paint paint;

    Bubble(int X, int Y, int Size, Paint paint){
        this.x = X;
        this.y = Y;
        this.size = Size;
        this.paint = new Paint();
        this.paint = paint;
    }

    public void draw(Canvas canvas){
        canvas.drawCircle(x, y, size, paint);
    }
}
