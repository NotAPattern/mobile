import static org.junit.jupiter.api.Assertions.*;

class TaskOneTest {

    @org.junit.jupiter.api.Test
    void testBooleanExpression_first(){

        boolean var_a = true;
        boolean var_b = false;
        boolean var_c = false;
        boolean var_d = false;
        assertFalse(TaskOne.booleanExpression(var_a, var_b, var_c, var_d));

    }

    @org.junit.jupiter.api.Test
    void testBooleanExpression_second(){

        final boolean var_a =true;
        final boolean var_b =false;
        final boolean var_c =true;
        final boolean var_d =false;
        assertTrue(TaskOne.booleanExpression(var_a, var_b, var_c, var_d));
    }

    @org.junit.jupiter.api.Test
    void booleanExpressionThirdTest(){
        final boolean var_a = true;
        final boolean var_b = true;
        final boolean var_c = false;
        final boolean var_d = false;

        assertTrue(TaskOne.booleanExpression(var_a, var_b, var_c, var_d));
    }

    @org.junit.jupiter.api.Test
    void booleanExpressionForthTest(){
        final boolean var_a = false;
        final boolean var_b = false;
        final boolean var_c = false;
        final boolean var_d = false;

        assertFalse(TaskOne.booleanExpression(var_a, var_b, var_c, var_d));
    }

    @org.junit.jupiter.api.Test
    void booleanExpressionFifthTest(){
        final boolean var_a = true;
        final boolean var_b = true;
        final boolean var_c = true;
        final boolean var_d = true;

        assertFalse(TaskOne.booleanExpression(var_a, var_b, var_c, var_d));
    }

}