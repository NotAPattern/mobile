import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskSevenTest {

    @Test
    void mergeArrays() {
        int[] arr1 = new int[0], arr2 = new int[0], arr3 = new int[0];
        assertArrayEquals(arr3, TaskSeven.mergeArrays(arr1, arr2));
        int[] arr4 = {0,1,2,3};
        int[] arr5 = {0,1,2,3};
        assertArrayEquals(new int[] {0, 0, 1, 1, 2, 2, 3, 3}, TaskSeven.mergeArrays(arr4, arr5));
    }
}