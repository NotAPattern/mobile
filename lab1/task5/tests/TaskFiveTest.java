import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskFiveTest {

    @Test
    void isPalindrome() {
        String test1 = "abc";
        assertFalse(TaskFive.isPalindrome(test1));
        String test2 = "aa";
        assertTrue(TaskFive.isPalindrome(test2));
        String test3 = "^%%&$% bbaabb*^";
        assertTrue(TaskFive.isPalindrome(test3));
    }
}