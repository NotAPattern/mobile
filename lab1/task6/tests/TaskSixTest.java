import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class TaskSixTest {

    @Test
    void factorial() {
        int value1 = 0;
        assertEquals(new BigInteger(String.valueOf(1)), TaskSix.factorial(value1));
        int value2 = 1;
        assertEquals(new BigInteger(String.valueOf(1)), TaskSix.factorial(value2));
        int value3 = 5;
        assertEquals(new BigInteger(String.valueOf(120)), TaskSix.factorial(value3));
        int value4 = 42;
        assertEquals(new BigInteger("1405006117752879898543142606244511569936384000000000"), TaskSix.factorial(value4));
    }
}