package com.example.lab3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    int[] win_strategy = new int[4];
    String output = "";
    String result = "";
    int moves = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refreshWinStrategyNumbers();
    }

    public void refreshWinStrategyNumbers(){
        for(int k = 0; k < win_strategy.length; k++){
            win_strategy[k] = (int) (Math.random() * 10);
        }
    }

    public void checkNumbers(View view) {
        TextView textView = (TextView) findViewById(R.id.textView);
        EditText textEdit = (EditText) findViewById(R.id.editTextNumberSigned);
        String[] numbers = new String[4];
        numbers = textEdit.getText().toString().split("");
        int[] array_of_numbers = new int [4];
        int[] mask_win_strategy = new int [4];
        int[] copy_win_strategy = win_strategy.clone();
        result = "";

        for(int i = 0; i < numbers.length; i++){
            array_of_numbers[i] = Integer.parseInt(numbers[i]);
        }

        for(int i = 0; i < array_of_numbers.length; i++){
            int test_number = array_of_numbers[i];
            int j = 0;
            if(test_number == win_strategy[i]){
                mask_win_strategy[i] = 2;
            } else {
                while(j < array_of_numbers.length){
                    if(test_number == win_strategy[j]){
                        if(mask_win_strategy[j] != 2 && mask_win_strategy[j] != 1){
                            mask_win_strategy[j] = 1;
                            j += win_strategy.length;
                        }
                    }
                    j++;
                }
            }
        }

        for(int i = 0; i < win_strategy.length; i++){
            if(mask_win_strategy[i] == 2){
                result += "В";
            } else if(mask_win_strategy[i] == 1){
                result += "К";
            }
        }

        moves++;

        if(result.equals("ВВВВ")) {
            output += moves + ". " + Arrays.toString(array_of_numbers) + " – " + result + " You win. Теперь новое число :)\n";
            textView.setText(output);
            textEdit.setText("");
            refreshWinStrategyNumbers();
        } else {
            output += moves + ". " + Arrays.toString(array_of_numbers) + " – " + result + "\n";
            textView.setText(output);
        }
    }
}