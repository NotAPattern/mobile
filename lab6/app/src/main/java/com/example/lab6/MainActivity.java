package com.example.lab6;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ActionBar actionBar;
    EditText inputText;
    EditText outputText;
    SeekBar seekBar;
    EditText editKey;
    Button button;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createAndSetActionBar();

        inputText = (EditText) findViewById(R.id.inputText);
        outputText = (EditText) findViewById(R.id.outputText);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        editKey = (EditText)findViewById(R.id.editKey);
        button = (Button) findViewById(R.id.button);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        editKey.setText(""+ (int)(seekBar.getProgress() - 13));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int key = seekBar.getProgress() - 13;
                String message = inputText.getText().toString();
                String output = encode(message, key);
                outputText.setText(output);
                editKey.setText("" + key);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        
        Intent receivedIntent = getIntent();
        String receivedText = receivedIntent.getStringExtra(Intent.EXTRA_TEXT);
        if (receivedText!= null) {
            inputText.setText(receivedText);
        }
        button.setOnClickListener(v -> {
            int key = Integer.parseInt(editKey.getText().toString());
            String message = inputText.getText().toString();
            String output = encode(message, key);
            outputText.setText(output);
        });

        fab.setOnClickListener(v -> {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Secret Message "+ DateFormat.getDateTimeInstance().format(new Date()));
            shareIntent.putExtra(Intent.EXTRA_TEXT, outputText.getText().toString());
            try {startActivity(Intent.createChooser(shareIntent, "Share message..."));
                finish();
            }
            catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, "Error: Couldn't share.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    void createAndSetActionBar(){
        actionBar = getSupportActionBar();
        actionBar.setTitle("Секретные сообщения");
    }

    public String encode(String message, int keyVal) {
        String output = "";
        char key = (char) keyVal;
        for (int x = 0; x < message.length(); x++) {
            char input = message.charAt(x);
            if (input >= 'А' && input <= 'Я') {
                input += key;
                if (input > 'Я')
                    input -= 26;
                if (input < 'А')
                    input += 26;
            } else if (input >= 'а' && input <= 'я') {
                input += key;
                if (input > 'я')
                    input -= 26;
                if (input < 'а')
                    input += 26;
            } else if (input >= '0' && input <= '9') {
                input += (keyVal % 10);
                if (input > '9')
                    input -= 10;
                if (input < '0')
                    input += 10;
            }
            output += input;
        }
        return output;
    }

}