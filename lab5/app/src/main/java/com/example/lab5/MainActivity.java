package com.example.lab5;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    long timer;
    private final static String TAG = "MainActivity";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
    @Override
    protected void onStop(){
        long currentTime = (Calendar.getInstance().getTimeInMillis() - timer) / (1000);
        this.timer = Calendar.getInstance().getTimeInMillis();
        super.onStop();
        Log.d(TAG, "onStop. Time onPause(in seconds): " + currentTime);
    }
    @Override
    protected void onStart(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long currentTime = (Calendar.getInstance().getTimeInMillis() - timer) / (1000);
        this.timer = Calendar.getInstance().getTimeInMillis();
        super.onStart();
        Log.d(TAG, "onStart. Time onRestart(in seconds): " + currentTime);
    }
    @Override
    protected void onPause(){
        long currentTime = (Calendar.getInstance().getTimeInMillis() - timer) / (1000);
        this.timer = Calendar.getInstance().getTimeInMillis();
        super.onPause();
        Log.d(TAG, "onPause. Time onResume(in seconds): " + currentTime);
    }
    @Override
    protected void onResume(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long currentTime = (Calendar.getInstance().getTimeInMillis() - timer) / (1000);
        this.timer = Calendar.getInstance().getTimeInMillis();
        super.onResume();
        Log.d(TAG, "onResume. Time onStart(in seconds): " + currentTime);
    }

    @Override
    protected void onRestart(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long currentTime = (Calendar.getInstance().getTimeInMillis() - timer) / (1000);
        this.timer = Calendar.getInstance().getTimeInMillis();
        super.onRestart();
        Log.d(TAG, "onRestart. Time onStop(in seconds): " + currentTime);
    }

    @Override
    protected  void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected  void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }
}